//I just create an array of jsonS that contains two users login credentials instead of working with a database
let usersLogin = [
    {
        "username": "karim",
        "password": "karim123"
    },
    {
        "username": "vneuron",
        "password": "vneuron"
    }
]

function login(){
    let user = document.getElementById("username").value
    let pass = document.getElementById("password").value
    if (!user || !pass){
        alert("Invalid Login ! Please check that you fill out all the fields!")
    }
    else if (pass.length < 5) {
        alert("Invalid Login ! Your password should contains at least 6 characters!")
    }
    else {
        let loginDone = false
        usersLogin.forEach((u) => {
            if (u.username == user && u.password == pass){
                window.open("edit.html", "_self")
                console.log("Login done")
                loginDone = true
            }
        })
        if (!loginDone){
            alert("Invalid Login !  Wrong credentials!")
        }
    }
}
